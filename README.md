# Facebook Connect
 
Script to integrate Facebook in a website! 

version 1.0

[Kogimobile.com][3]
 
## API

### FacebookConnect()

To initialize facebook connect you need to instantiate the `FacebookConnect` class.
 
#### Parameter `appId`
 
appId is a code. You can find in app config
 
#### Example
 
    var app = new FacebookConnect("appId");  
    
### init()

To initialize the FacebookConnect you need to call this method;

#### Example
 
    app.init();
    
### getStatusLogin()

You need to check if user is logged or has the app installed with allowed permissions 

This method return a String:

`connected`: user is logged and app is intalled

`not_authorized`: is logged and app isn't installed

`not_logged_in`: user is no logged

#### Parameter `callback`
 
Function is called after get status login
 
#### Example
 
    app.getStatusLogin(function(status) {
        console.log("status: " + status);
    });
 
### loginFacebook()

If user isn't logged or app isn't installed; call this method

#### Parameter `scope`
 
String separated by "," with app permissions

Permissions for Users

`email`
`publish_actions`
`user_about_me`
`user_actions.music`
`user_actions.news`
`user_actions.video`
`user_activities`
`user_birthday`
`user_education_history`
`user_events`
`user_games_activity`
`user_groups`
`user_hometown`
`user_interests`
`user_likes`
`user_location`
`user_notes`
`user_photos`
`user_questions`
`user_relationship_details`
`user_relationships`
`user_religion_politics`
`user_status`
`user_subscriptions`
`user_videos`
`user_website`
`user_work_history`

Permissions for friends

`friends_about_me`
`friends_actions.music`
`friends_actions.news`
`friends_actions.video`
`friends_activities`
`friends_birthday`
`friends_education_history`
`friends_events`
`friends_games_activity`
`friends_groups`
`friends_hometown`
`friends_interests`
`friends_likes`
`friends_location`
`friends_notes`
`friends_photos`
`friends_questions`
`friends_relationship_details`
`friends_relationships`
`friends_religion_politics`
`friends_status`
`friends_subscriptions`
`friends_videos`
`friends_website`
`friends_work_history`

Permissions for extended permissions
	
`ads_management`
`create_event`
`create_note`
`export_stream`
`friends_online_presence`
`manage_friendlists`
`manage_notifications`
`manage_pages`
`offline_access`
`photo_upload`
`publish_checkins`
`publish_stream`
`read_friendlists`
`read_insights`
`read_mailbox`
`read_page_mailboxes`
`read_requests`
`read_stream`
`rsvp_event`
`share_item`
`sms`
`status_update`
`user_online_presence`
`video_upload`
`xmpp_login`

Note:

Probably some permissions change in the future, please check the list.

You can see all permissions at this link:

[https://www.developers.facebook.com/tools/explorer/][1]

You can see the api user in facebook at this link:

[https://www.developers.facebook.com/docs/reference/api/user/][2]
 
##### Parameter `redirect_uri`

Redirect method when user install app or is logged
 
#### Example

	app.loginFacebook(scope, redirect_uri);
	
### getDataUser()

This method return an Object

#### Parameter `attributes`

String separated by "," with the attributs that you need get. Default is `id`,`name`.

Attributes:

`about`
`address`
`age_range`
`bio`
`birthday`
`cover`
`currency`
`devices`
`education`
`email`
`favorite_athletes`
`favorite_teams`
`first_name`
`gender`
`hometown`
`id`
`inspirational_people`
`install_type`
`installed`
`interested_in`
`languages`
`last_name`
`link`
`locale`
`location`
`meeting_for`
`middle_name`
`name`
`name_format`
`political`
`picture`
`quotes`
`relationship_status`
`religion`
`security_settings`
`significant_other`
`sports`
`third_party_id`
`timezone`
`updated_time`
`username`
`verified`
`video_upload_limits`
`viewer_can_send_gift`
`website`
`inbox`
`scores`
 
##### Parameter `callback`
 
Function called after get data user
 
#### Example

	app.getDataUser(attributes, callback(data));
	
### getDataFriends()

This method return an Object

#### Parameters `options`

Object with options to get data friends.

The attributes for the param options are:

`getFriends`: You can give as value "all" or "installed". Default is: "all".

All: to get all my friends.

Installed: to get my friends with our app installed

`limit`: Define the limit of response: number or "all": Default is: "all".

`dataToGet`: Define attributes to get of my friends. Default is: `id`,`name`.

The attribus are:

`cover`
`devices`
`education`
`favorite_teams`
`first_name`
`id`
`install_type`
`installed`
`last_name`
`link`
`locale`
`name`
`name_format`
`picture`
`quotes`
`third_party_id`
`updated_time`
`username`
`website`
`work`

`getStatusFriends`: Boolean. Default is false

Note:

This attribut return three diferents status: 

`offline`: User isn't logged in facebook chat 

`online`: User in online in facebook chat

`idle`: when user has been inactive for at least 10 consecutive minutes.

#### Parameter `callback`

Function called after get data friends
 
#### Example

	app.getDataFriends(options, callback(data));
	
### dataFriends()

This method return an Object

#### Parameters `options`

Object with options to get data friends.

The attributes for the param options are:

`id`: You can give as value "all" or "installed". Default is: "all".

`attributes`: Define attributes to get of my friends. Default is: `id`,`name`.

The attribus are:

`about`
`address`
`age_range`
`bio`
`birthday`
`cover`
`currency`
`devices`
`education`
`email`
`favorite_athletes`
`favorite_teams`
`first_name`
`gender`
`hometown`
`id`
`inspirational_people`
`install_type`
`installed`
`interested_in`
`languages`
`last_name`
`link`
`locale`
`location`
`meeting_for`
`middle_name`
`name`
`name_format`
`political`
`picture`
`quotes`
`relationship_status`
`religion`
`security_settings`
`significant_other`
`sports`
`third_party_id`
`timezone`
`updated_time`
`username`
`verified`
`video_upload_limits`
`viewer_can_send_gift`
`website`
`inbox`
`scores`

#### Parameter `callback`

Function called after get data friends
 
#### Example

	app.dataFriends(options, callback(data));
	
### comment()

Redirect to facebook for comment.

#### Parameters `path`

Is a object with attributs required.

The attributes for the param path are:

`link`: link to post in the timeline

`picture`: add image, default is picture of app

`name`: name of app

`caption`: small caption

`description`: need a description

`redirect_uri`: redirect after comment
 
#### Example

	app.comment({
		link: "http://www.kogimobile.com/",
     	picture: "http://fbrell.com/f8.jpg",
     	name: "Chumager",
     	caption: "Game",
     	description: "I have new ranking",
     	redirect_uri: "http://www.kogimobile.com/html5/facebook"
	});
	
### logoutFacebook()

Logout of facebook, This method return a Object

#### Parameter `callback`

Function called after logout

#### Example

	 app.logoutFacebook(function(data) {
    	console.log(data);
    });

	
[1]: https://developers.facebook.com/tools/explorer/
[2]: https://developers.facebook.com/docs/reference/api/user/
[3]: http://www.kogimobile.com/